D := docker
DC := $(D) compose
CONTAINER := kata_php_app
IMAGE := kata_php_image
DC_EXEC := $(DC) exec $(CONTAINER)

create-image:
	@$(D) build -t $(IMAGE) .

remove-image:
	@$(D) image rm $(IMAGE) -f

install:
	@$(DC_EXEC) composer install

start:
	@$(DC) up -d

bash:
	@$(D) exec -it $(CONTAINER) bash

stop:
	@$(DC) stop

phpunit:
	@$(DC_EXEC) vendor/bin/phpunit tests

phpstan:
	@$(DC_EXEC) vendor/bin/phpstan analyse -l 9 src
