<?php

namespace tests;

use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use src\Entity\Quote;
use src\Entity\Template;
use src\Entity\User;
use src\TemplateManager;

require_once __DIR__ . '/../src/Entity/Destination.php';
require_once __DIR__ . '/../src/Entity/Quote.php';
require_once __DIR__ . '/../src/Entity/Site.php';
require_once __DIR__ . '/../src/Entity/Template.php';
require_once __DIR__ . '/../src/Entity/User.php';
require_once __DIR__ . '/../src/Helper/SingletonTrait.php';
require_once __DIR__ . '/../src/Helper/NeedleTrait.php';
require_once __DIR__ . '/../src/Helper/ClassResolverTrait.php';
require_once __DIR__ . '/../src/Context/ApplicationContext.php';
require_once __DIR__ . '/../src/Repository/Repository.php';
require_once __DIR__ . '/../src/Repository/DestinationRepository.php';
require_once __DIR__ . '/../src/Repository/QuoteRepository.php';
require_once __DIR__ . '/../src/Repository/SiteRepository.php';
require_once __DIR__ . '/../src/TemplateManager.php';
require_once __DIR__ . '/../src/Service/HandlerInterface.php';
require_once __DIR__ . '/../src/Service/UserHandler.php';
require_once __DIR__ . '/../src/Service/QuoteHandler.php';

class TemplateManagerTest extends TestCase
{
    private Generator $faker;
    private TemplateManager $templateManager;

    /**
     * Init the mocks
     */
    public function setUp(): void
    {
        $this->faker = Factory::create();
        $this->templateManager = new TemplateManager();
    }

    /**
     * Closes the mocks
     */
    public function tearDown(): void
    {
    }

    public function testUserShouldBeNotBeOverridden(): void
    {
        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[user:first_name]"),
            []
        );

        $this->assertStringNotContainsStringIgnoringCase("[user:first_name]", $message->content);
    }

    public function testUserShouldBeOverridden(): void
    {
        $user = new User($this->faker->randomNumber(), 'fisrtname', 'fastname', 'email@gmail.com');

        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[user:first_name]"),
            [
                'user' => $user
            ]
        );

        $this->assertStringContainsString(ucfirst($user->firstname), $message->content);
    }


    public function testDestinationLinkShouldBeRemoved(): void
    {
        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "shouldBeNoDestinationLeft[quote:destination_link]"),
            []
        );

        $this->assertStringContainsString('shouldBeNoDestinationLeft', $message->content);
    }

    public function testDestinationLinkShouldBeReplaced(): void
    {
        $quote = new Quote($this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->dateTime());

        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[quote:destination_link]"),
            [
                'quote' => $quote
            ]
        );

        $this->assertNotEmpty($message->content);
    }


    public function testSummaryShouldBeDisplayed(): void
    {
        $quote = new Quote($this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->dateTime());

        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[quote:summary]"),
            [
                'quote' => $quote
            ]
        );

        $this->assertNotEmpty($message->content);
        $this->assertStringNotContainsStringIgnoringCase("[quote:summary]", $message->content);
    }

    public function testSummaryShouldNotBeReplaced(): void
    {
        // This behavior might need to be changed is the main function
        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[quote:summary]"),
            []
        );

        $this->assertStringContainsString("[quote:summary]", $message->content);
    }


    public function testSummaryHtmlShouldBeDisplayed(): void
    {
        $quote = new Quote($this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->dateTime());

        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[quote:summary_html]"),
            [
                'quote' => $quote
            ]
        );

        $this->assertNotEmpty($message->content);
        $this->assertStringContainsString('<p>', $message->content);
        $this->assertStringNotContainsStringIgnoringCase("[quote:summary_html]", $message->content);
    }

    public function testSummaryHtmlShouldNotBeReplaced(): void
    {
        // This behavior might need to be changed is the main function
        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[quote:summary_html]"),
            []
        );

        $this->assertStringContainsString("[quote:summary_html]", $message->content);
    }


    public function testDestinationNameShouldBeDisplayed(): void
    {
        $quote = new Quote($this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->dateTime());

        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[quote:destination_name]"),
            [
                'quote' => $quote
            ]
        );

        $this->assertNotEmpty($message->content);
        $this->assertStringNotContainsStringIgnoringCase("[quote:destination_name]", $message->content);
    }

    public function testDestinationNameShouldNotBeReplaced(): void
    {
        // This behavior might need to be changed is the main function
        $message = $this->templateManager->getTemplateComputed(
            new Template(1, 'subject', "[quote:destination_name]"),
            []
        );

        $this->assertNotEmpty($message->content);
        $this->assertStringContainsString("[quote:destination_name]", $message->content);
    }
}
