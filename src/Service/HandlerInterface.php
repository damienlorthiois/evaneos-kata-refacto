<?php

namespace src\Service;

interface HandlerInterface
{
    /**
     * @param array<object> $data
     */
    public function handle(string $text, array $data): string;
    public static function getInstance(): self;
}
