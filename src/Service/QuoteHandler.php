<?php

namespace src\Service;

use src\Entity\Quote;
use src\Helper\NeedleTrait;
use src\Helper\SingletonTrait;
use src\Repository\DestinationRepository;
use src\Repository\QuoteRepository;
use src\Repository\SiteRepository;

class QuoteHandler implements HandlerInterface
{
    use SingletonTrait;
    use NeedleTrait;

    private QuoteRepository $quoteRepository;
    private SiteRepository $siteRepository;
    private DestinationRepository $destinationRepository;

    public function __construct()
    {
        $this->quoteRepository = QuoteRepository::getInstance();
        $this->siteRepository = SiteRepository::getInstance();
        $this->destinationRepository = DestinationRepository::getInstance();
    }

    /**
     * @param array<object> $data
     */
    public function handle(string $text, array $data): string
    {
        $quote = (isset($data['quote']) and $data['quote'] instanceof Quote) ? $data['quote'] : null;

        if ($quote) {
            $_quoteFromRepository = $this->quoteRepository->getById($quote->id);
            $usefulObject = $this->siteRepository->getById($quote->siteId);
            $destinationOfQuote = $this->destinationRepository->getById($quote->destinationId);

            $text = $this->handleNeedle($text, '[quote:summary_html]', Quote::renderHtml($_quoteFromRepository));
            $text = $this->handleNeedle($text, '[quote:summary]', Quote::renderText($_quoteFromRepository));
            $text = $this->handleNeedle($text, '[quote:destination_name]', $destinationOfQuote->countryName);
            $text = $this->handleNeedle($text, '[quote:destination_link]', $usefulObject->url . '/' . $destinationOfQuote->countryName . '/quote/' . $_quoteFromRepository->id);
        }

        return $this->removeNeedle($text, '[quote:destination_link]');
    }
}
