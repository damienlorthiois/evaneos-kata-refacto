<?php

namespace src\Service;

use src\Context\ApplicationContext;
use src\Entity\User;
use src\Helper\NeedleTrait;
use src\Helper\SingletonTrait;

class UserHandler implements HandlerInterface
{
    use SingletonTrait;
    use NeedleTrait;

    private ApplicationContext $applicationContext;

    public function __construct()
    {
        $this->applicationContext = ApplicationContext::getInstance();
    }

    /**
     * @param array<object> $data
     */
    public function handle(string $text, array $data): string
    {
        $_user = (isset($data['user']) and ($data['user'] instanceof User)) ? $data['user'] : $this->applicationContext->getCurrentUser();

        return $this->handleNeedle($text, '[user:first_name]', ucfirst(mb_strtolower($_user->firstname)));
    }
}
