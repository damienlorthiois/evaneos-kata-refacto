<?php

namespace src\Repository;

use Faker;
use src\Entity\Site;
use src\Helper\SingletonTrait;

/**
 * @implements Repository<Site>
 */
class SiteRepository implements Repository
{
    use SingletonTrait;

    public function getById(int $id)
    {
        // DO NOT MODIFY THIS METHOD
        $faker = Faker\Factory::create();
        $faker->seed($id);

        return new Site($id, $faker->url);
    }
}
