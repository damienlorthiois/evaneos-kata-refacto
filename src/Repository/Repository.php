<?php

namespace src\Repository;

/**
 * @template T
 */
interface Repository
{
    /**
     * @return T
     */
    public function getById(int $id);
}
