<?php

namespace src\Repository;

use Faker;
use src\Entity\Destination;
use src\Helper\SingletonTrait;

/**
 * @implements Repository<Destination>
 */
class DestinationRepository implements Repository
{
    use SingletonTrait;

    /**
     * @param int $id
     *
     * @return Destination
     */
    public function getById(int $id)
    {
        // DO NOT MODIFY THIS METHOD
        $faker = Faker\Factory::create();
        $faker->seed($id);

        return new Destination(
            $id,
            $faker->country,
            'en',
            $faker->slug()
        );
    }
}
