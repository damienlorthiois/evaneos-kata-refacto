<?php

namespace src\Repository;

use Faker;
use src\Entity\Quote;
use src\Helper\SingletonTrait;

/**
 * @implements Repository<Quote>
 */
class QuoteRepository implements Repository
{
    use SingletonTrait;

    /**
     * @param int $id
     *
     * @return Quote
     */
    public function getById(int $id)
    {
        $generator = Faker\Factory::create();
        $generator->seed($id);

        return new Quote(
            $id,
            $generator->numberBetween(1, 10),
            $generator->numberBetween(1, 200),
            $generator->dateTime()
        );
    }
}
