<?php

namespace src;

use src\Entity\Template;
use src\Helper\ClassResolverTrait;
use src\Service\HandlerInterface;

class TemplateManager
{
    use ClassResolverTrait;

    /**
     * @var array<int, class-string>
     */
    private array $classes;

    public function __construct()
    {
        $this->classes = $this->getImplementingClasses(HandlerInterface::class);
    }

    /**
     * @param array<object> $data
     * @return Template
     */
    public function getTemplateComputed(Template $tpl, array $data)
    {
        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    /**
     * @param array<object> $data
     */
    private function computeText(string $text, array $data): string
    {
        foreach ($this->classes as $class) {
            /** @var HandlerInterface $class */
            $text = $class::getInstance()->handle($text, $data);
        }

        return $text;
    }
}
