<?php

namespace src\Helper;

trait SingletonTrait
{
    /**
     * @var self
     */
    private static $instance = null;

    public static function getInstance(): self
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
