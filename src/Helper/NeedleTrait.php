<?php

namespace src\Helper;

trait NeedleTrait
{
    public function handleNeedle(string $text, string $needle, string $replace): string
    {
        if (!str_contains($text, $needle)) {
            return $text;
        }

        return str_replace($needle, $replace, $text);
    }

    public function removeNeedle(string $text, string $needle): string
    {
        return str_replace($needle, '', $text);
    }
}
