<?php

namespace src\Helper;

use src\Service\HandlerInterface;

trait ClassResolverTrait
{
    /**
     * @return array<int, class-string>
     */
    public function getImplementingClasses(string $interfaceName): array
    {
        return array_filter(
            get_declared_classes(),
            function ($className) use ($interfaceName) {
                return in_array($interfaceName, class_implements($className));
            }
        );
    }
}
