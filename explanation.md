# Refactoring Kata Test


## Docker

Dans un premier temps, j'ai ajouté docker pour permettre de faire tourner le projet localement.

Une fois le container lancé, on peut accéder à l'app ici => `http://127.0.0.1:8082` 

Les ports 8080 et 8081 étaient déjà pris.

J'ai modifié la conf apache via le dockerfile pour qu'il puisse pointer directement sur le dossier public

## PHP & Dépendances

J'ai update la version de php => 8.2 puis update les dépendances.

## Tests

Je ne suis pas fan de la TDD pour le développement de nouvelles fonctionnalités.

Cependant elle est essentielle pour de la refacto.

Ca m'a permis de mieux apréhender le traitement de la fonction et de m'assurer qu'aucune regression puisse être présente une fois la refacto effectuée.

## Refacto

J'ai commencé par une refacto basique, pour y voir un peu mieux dans la fonction.

Puis j'ai ajouté la partie handlerInterface, permettant dans un "futur éventuel" de faire évoluer le scope des objets à traduire de facon plus pérenne.

## PHP CS Fixer

Amélioration du Code Style

## PHPSTAN

J'ai dans un premier temps update les signatures de toutes les fonctions, exceptée `getTemplateComputed` qui ne devait pas être touchée.

Puis j'ai ajouté phpstan pour m'assurer que je n'avais rien oublié et surtout de prévenir de bugs que je n'aurais pas vu..
